function CrearInmediatos() {

	var precio, resultado1

	precio = 22
	resultado1 = document.getElementById('resultado')
	resultado1.innerHTML += "<br> La variable precio vale: " + precio
}

function AumentarVariables() {

	var numero, valor, resultado2

	valor = 4
	numero = valor++
	resultado2 = document.getElementById('resultadoAumentar')
	resultado2.innerHTML += "<br> Con valor++ las varibles valen <br>" +
							" Valor: " + valor + "<br>Número: " + numero

	numero = 0
	valor = 4
	numero = ++valor
	resultado2 = document.getElementById('resultadoAumentar')
	resultado2.innerHTML += "<br> Con ++valor las varibles valen <br>" +
							" Valor: " + valor + "<br>Número: " + numero
}