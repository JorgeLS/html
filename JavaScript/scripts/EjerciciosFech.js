function Ejercicio1() {
	var date1 = document.getElementById('date1.1').value
	var date2 = document.getElementById('date1.2').value
	var resultado = document.getElementById('resultadoEj1')

	if (date1 > date2)
		resultado.innerHTML = "La fecha 1 es mas nueva que es " + date1 
	else if (date1 < date2)
		resultado.innerHTML = "La fecha 2 es mas nueva que es " + date2
	else
		resultado.innerHTML = "La fecha 1 es la misma que la fecha 2 que son " + date2
}

function Ejercicio2() {
	var day1 = document.getElementById('date2.1.day1').value
	var month1 = document.getElementById('date2.1.month1').value
	var year1 = document.getElementById('date2.1.year1').value
	var day2 = document.getElementById('date2.1.day2').value
	var month2 = document.getElementById('date2.1.month2').value
	var year2 = document.getElementById('date2.1.year2').value
	var text = document.getElementById('resultadoEj2')
	var resultado

	if( (day1 > 0 && day1 < 32) && (month1 > 0 && month1 < 13) && year1 < year2 &&
		(day2 > 0 && day2 < 32) && (month2 > 0 && month2 < 13) ) {
		
		if (month1 >= month2) {
			if(day1 > day2)
				resultado = year2 - year1 - 1
			else
				resultado = year2 - year1
		}
		else
			resultado = year2 - year1

		text.innerHTML = "Tienes: " + resultado + " años"
	}
	else
		text.innerHTML = "La fecha esta incorrecta o no has nacido"
}

function Ejercicio3() {

	var day = parseInt (document.getElementById('date3.1').value)
	var month = parseInt (document.getElementById('date3.2').value)
	var year = parseInt (document.getElementById('date3.3').value)
	var text = document.getElementById('resultadoEj3')
	var BI_day
	var last_day
	// text.innerHTML = day + "-" + month + "-" + year + <br>

	if( (day > 0 && day < 32) && (month > 0 && month < 13)) { //&& (month == 2 && day != 30) && (year % 4 != 0 && day != 29)) {
		
		if (year % 4 == 0)
			BI_day = 29
		else if (year % 4 != 0)
			BI_day = 28
		else {
			day = 0
			text.innerHTML = "La fecha es incorrecta"
		}

		if (day) {
			switch(month) {
				case 1: case 3: case 5: case 7: case 8: case 10: case 12:
					last_day = 31					
					break

				case 4: case 6: case 9: case 11:
					if (day > 30) {
						text.innerHTML = "La fecha es incorrecta"
						break;
					}
					last_day = 30
					break

				case 2:
					if (day > BI_day) {
						text.innerHTML = "La fecha es incorrecta"
						break
					}
					last_day = BI_day
					break
				default:
					break
			}

			if(day == last_day) {
				day = 1
				month++
			}
			else
				day++

			if(month == 13) {
				year++
				month = 1
			}

			if(!(day > BI_day))
				text.innerHTML = day + "-" + month + "-" + year
		}
		else
			text.innerHTML = "La fecha es incorrecta"
	}
	else
		text.innerHTML = "La fecha es incorrecta"
}