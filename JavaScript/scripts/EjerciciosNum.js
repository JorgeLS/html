function Ejercicio1() {
	var num1 = document.getElementById('num1.1').value
	var num2 = document.getElementById('num1.2').value
	var num3 = document.getElementById('num1.3').value
	var num4 = document.getElementById('num1.4').value
	var resultado = document.getElementById('resultadoEj1')

	if ( num1 > num2 && num1 > num3 && num1 > num4 )
		resultado.innerHTML = "El " + num1 + " que es el numero 1, es el mayor de todos"
	else if ( num2 > num1 && num2 > num3 && num2 > num4 )
		resultado.innerHTML = "El " + num2 + " que es el numero 2, es el mayor de todos" 
	else if ( num3 > num1 && num3 > num2 && num3 > num4 )
		resultado.innerHTML = "El " + num3 + " que es el numero 3, es el mayor de todos" 
	else
		resultado.innerHTML = "El " + num4 + " que es el numero 4, es el mayor de todos"
}

function Ejercicio2() {
	var num1 = document.getElementById('num2.1').value
	var num2 = document.getElementById('num2.2').value
	var num3 = document.getElementById('num2.3').value
	var resultado = document.getElementById('resultadoEj2')

	if(num1 > num2 && num1 > num3) {
		if(num2 > num3)
			resultado.innerHTML = "El " + num1 + " que es el número 1, es el mayor de todos y el menor es número 3"
		else
			resultado.innerHTML = "El " + num1 + " que es el número 1, es el mayor de todos y el menor es número 2"
	}
	else if (num2 > num1 && num2 > num3) {
		if(num1 > num3)
			resultado.innerHTML = "El " + num2 + " que es el número 2, es el mayor de todos y el menor es número 3"
		else
			resultado.innerHTML = "El " + num2 + " que es el número 2, es el mayor de todos y el menor es número 1"
	}
	else {
		if(num1 > num2)
			resultado.innerHTML = "El " + num3 + " que es el número 3, es el mayor de todos y el menor es número 2"
		else
			resultado.innerHTML = "El " + num3 + " que es el número 3, es el mayor de todos y el menor es número 1"
	}
}