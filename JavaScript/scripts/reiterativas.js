function Calculanum() {
	var mes = parseInt(document.getElementById('numero1').value)
	var resultado = document.getElementById('resultado1')

	switch(mes) {
		case 1: case 3: case 5: case 7: case 8: case 10: case 12: 
			resultado.innerHTML += "<br> Tiene 31 días el mes " + mes 
			break;
		case 4: case 6: case 9: case 11:
			resultado.innerHTML += "<br> Tiene 30 días el mes " + mes
			break;
		case 2:
			resultado.innerHTML += "<br> Tiene 29 días el mes " + mes + " o si es un año bisiesto 28"
			break;
		default:
			resultado.innerHTML += "<br> Introduce un mes"
			break;
	}
}

function tablaMulti() {
	var num = parseInt (document.getElementById('numero2').value)
	var resultado = document.getElementById('resultado2')
	resultado.innerHTML = " " 
	var n = 0

	resultado.innerHTML += "Tabla con While <br>"
	while(n < 11) {
		resultado.innerHTML += num + " x " + n + " = " + (num * n) + "<br>"
		n++ 
	}

	resultado.innerHTML += "<br>Tabla con For <br>"
	for (n=0; n<11; n++)
		resultado.innerHTML += num + " x " + n + " = " + (num * n) + "<br>"

	resultado.innerHTML += "<br>Tabla con Do While <br>"
	n=0
	do{
		resultado.innerHTML += num + " x " + n + " = " + (num * n) + "<br>"
		n++ 
	}while(n < 11) 

}

function EscribirDias() 
{

	var tablaDias = new Array()
	var resultado = document.getElementById('resultadoDias')
	var contador
	resultado.innerHTML = " "

	tablaDias[0] = "Lunes"
	tablaDias[1] = "Martes"
	tablaDias[2] = "Miercoles"
	tablaDias[3] = "Jueves"
	tablaDias[4] = "Viernes"
	tablaDias[5] = "Sábado"
	tablaDias[6] = "Domingo"

	for(contador=0; contador<5; contador++)
		resultado.innerHTML += "El dia " + contador + " es " + tablaDias[contador] + "<br>"

	for(contador in tablaDias)
		resultado.innerHTML += "El dia " + contador + " es " + tablaDias[contador] + "<br>"
}