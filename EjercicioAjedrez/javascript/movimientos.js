function movimientos (x) {

	var choque = false;

	// Cada ficha tiene su número que se guarda en num_ficha
	switch (num_ficha) {

		// PEÓN
		case 0:
			// En el caso del peón se mueve hacia arriba o hacia abajo dependiendo del color
			if (color == "negro")
				// Abajo
				marca_movimiento_peon ( parseInt(fila)+1, columna );
			else
				//Arriba
				marca_movimiento_peon ( parseInt(fila)-1, columna );

			break;

		// En los siguientes casos se hará un bucle para cada dirección de la ficha a la que se puede mover
		// El if comprueba que no se pase del tablero
		// Si no se pasa llama a marca_movimiento y si choca con otra pieza pone a choque = true que acaba con el bucle
		// Hace lo mismo que en toda la ficha pero cambiando la dirección
		
		// TORRE
		case 1:
			// Dir. Abajo
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)+i) >= DIM ) break;
				choque = marca_movimiento( parseInt(fila)+i, columna );
			}
			choque = false;

			// Dir. Arriba
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)-i) < 0 ) break;
				choque = marca_movimiento( parseInt(fila)-i, columna );
			}
			choque = false;

			// Dir. Derecha
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(columna)+i) >= DIM ) break;
				choque = marca_movimiento( fila, parseInt(columna)+i );
			}
			choque = false;

			// Dir. Izquierda
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(columna)-i) < 0 ) break;
				choque = marca_movimiento( fila, parseInt(columna)-i );
			}
			choque = false;

			break;

		// ALFIL
		case 2:

			// Dir. Abajo derecha
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)+i) >= DIM || (parseInt(columna)+i) >= DIM ) break;
				choque = marca_movimiento( parseInt(fila)+i, parseInt(columna)+i );
			}
			choque = false;

			// Dir. Abajo izquierda
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)+i) >= DIM || (parseInt(columna)-i) < 0 ) break;
				choque = marca_movimiento( parseInt(fila)+i, parseInt(columna)-i );
			}
			choque = false;

			// Dir. Arriba derecha
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)-i) < 0 || (parseInt(columna)+i) >= DIM ) break;
				choque = marca_movimiento( parseInt(fila)-i, parseInt(columna)+i );
			}
			choque = false;

			// Dir. Arriba izquierda
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)-i) < 0 || (parseInt(columna)-i) < 0 ) break;
				choque = marca_movimiento( parseInt(fila)-i, parseInt(columna)-i );
			}
			choque = false;
			
			break;

		// REINA
		case 3:
			// MOV COMO TORRE
			// Dir. Abajo
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)+i) >= DIM ) break;
				choque = marca_movimiento( parseInt(fila)+i, columna );
			}
			choque = false;

			// Dir. Arriba
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)-i) < 0 ) break;
				choque = marca_movimiento( parseInt(fila)-i, columna );
			}
			choque = false;

			// Dir. Derecha
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(columna)+i) >= DIM ) break;
				choque = marca_movimiento( fila, parseInt(columna)+i );
			}
			choque = false;

			// Dir. Izquierda
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(columna)-i) < 0 ) break;
				choque = marca_movimiento( fila, parseInt(columna)-i );
			}
			choque = false;

			// MOV COMO ALFIL
			// Dir. Abajo derecha
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)+i) >= DIM || (parseInt(columna)+i) >= DIM ) break;
				choque = marca_movimiento( parseInt(fila)+i, parseInt(columna)+i );
			}
			choque = false;

			// Dir. Abajo izquierda
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)+i) >= DIM || (parseInt(columna)-i) < 0 ) break;
				choque = marca_movimiento( parseInt(fila)+i, parseInt(columna)-i );
			}
			choque = false;

			// Dir. Arriba derecha
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)-i) < 0 || (parseInt(columna)+i) >= DIM ) break;
				choque = marca_movimiento( parseInt(fila)-i, parseInt(columna)+i );
			}
			choque = false;

			// Dir. Arriba izquierda
			for (var i = 1; i < DIM && !choque; i++) {
				if( (parseInt(fila)-i) < 0 || (parseInt(columna)-i) < 0 ) break;
				choque = marca_movimiento( parseInt(fila)-i, parseInt(columna)-i );
			}
			choque = false;

			break;

		// REY
		case 4:
			// Al ser el rey no hace falta un bucle porque solo se mueve una posición para todas las direcciones

			// MOV COMO TORRE
			// Dir. Abajo
			if( (parseInt(fila)+1) < DIM )
				marca_movimiento( parseInt(fila)+1, columna );
			// Dir. Arriba
			if( (parseInt(fila)-1) >= 0 )
				marca_movimiento( parseInt(fila)-1, columna );
			// Dir. Derecha
			if( (parseInt(columna)+1) < DIM )
				marca_movimiento( fila, parseInt(columna)+1 );
			// Dir. Izquierda
			if( (parseInt(columna)-1) >= 0 )
				marca_movimiento( fila, parseInt(columna)-1 );

			// MOV COMO ALFIL
			// Dir. Abajo derecha
			if( (parseInt(fila)+1) < DIM && (parseInt(columna)+1) < DIM )
				marca_movimiento( parseInt(fila)+1, parseInt(columna)+1 );
			// Dir. Abajo izquierda
			if( (parseInt(fila)+1) < DIM && (parseInt(columna)-1) >= 0 )
				marca_movimiento( parseInt(fila)+1, parseInt(columna)-1 );
			// Dir. Arriba derecha
			if( (parseInt(fila)-1) >= 0 && (parseInt(columna)+1) < DIM )
				marca_movimiento( parseInt(fila)-1, parseInt(columna)+1 );
			// Dir. Arriba izquierda
			if( (parseInt(fila)-1) >= 0 && (parseInt(columna)-1) >= 0 )
				marca_movimiento( parseInt(fila)-1, parseInt(columna)-1 );

			break;
	}
}

function marca_movimiento (row, column) {

	// Coje la celda con la row(fila) y la column(columna) determinada
	var celda = document.getElementsByClassName(" f"+ row +" c"+ column )[0];

	// Si en la celda no hay ficha dibuja fondo azul
	if ( celda.innerHTML == "") {
		celda.style.backgroundColor = "#3333CC";
		return false;
	}
	// Si la celda hay ficha y es de distinto color dibuja fondo rojo 
	else if ( celda.innerHTML != "" && celda.innerHTML.indexOf(color) == -1 ) {
		celda.style.backgroundColor = "#DD6666";
		return true;
	}
	// Si la celda hay ficha pero es el mismo color no dibuja fondo pero devuelve true
	else
		return true;
}

function marca_movimiento_peon (row, column) {

	// Coje la celda con la row(fila) y la column(columna) determinada
	var celda = document.getElementsByClassName(" f"+ row +" c"+ column )[0];

	// Para Mover
	// Si en la celda no hay ficha dibuja fondo azul
	if ( celda.innerHTML == "" )
		celda.style.backgroundColor = "#3333CC";

	// Para Comer
	// Primero comprueba si se pasa del tablero y si no llama a marca_comer_peon

	if ( column+1 < DIM)
		marca_comer_peon (row, column+1);

	if ( column-1 >= 0 )
		marca_comer_peon (row, column-1);
}

function marca_comer_peon (row, column) {

	// Llama a la celda específica
	celda = document.getElementsByClassName(" f"+ row +" c"+ column)[0];

	// Si hay ficha y es distinta cambia el fondo a rojo
	if ( celda.innerHTML != "" && celda.innerHTML.indexOf(color) == -1 )
		celda.style.backgroundColor = "#DD6666";
}