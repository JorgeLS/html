// Varibles para cojer los ID del HTML
var menu
var tablero
// Nombre de cada ficha guardado en un array
var fichas = ["peon", "torre", "alfil", "reina", "rey"];
// Nombre de ficha
var nom_ficha
// Número de la ficha
var num_ficha
// Color de la ficha
var color

// Dimension del tablero
var DIM = 8

// Variables para cojer la fila y la columna
var get_fila
var get_columna
var fila
var columna

function dibuja_tablero () {

	// Función para crear el tablero
	tablero = document.getElementById('tablero');
	var filas;
	
	// Primero crea en la etiqueta tablero 8 filas (div)
	for(var i = 0; i < 8; i++) {
		tablero.innerHTML += "<div class=\"fila f" + i + "\">";
	}

	// Después por cada fila crea 8 columnas (div) y cada columna de un color determinado y sus funciones
	for(var i = 0; i < 8; i++) {
		filas  = document.getElementsByClassName('fila')[i];
		for (var j = 0; j < 8; j++) {
			if ( i % 2 == ( j % 2) )
				filas.innerHTML += "<div class=\"columna f" + i + " c" + j + " blanco\" " + 
						"style=\"background-color:;\" " +
						// Al poner el ratón encima del div se llama a la funcion dime_pos() con una fila y columna determinada y tambien llama a limpia_fondo()
						"onmouseover=\"dime_pos("+ i +", "+ j +"), limpia_fondo()\" " +
						// Al hacer clic en el div se llama a la funcion pon_pos() y tambien get_ficha() que le mete el this para saber que div está utilizando
						"onclick=\"pon_pos(), get_ficha(this)\" " +
						// Al hacer doble clic al div llama a la función quitar_ficha() y le da la información del div en el que esta con el this
						"ondblclick=\"quitar_ficha(this)\" " +
						"></div>";
			else
				filas.innerHTML += "<div class=\"columna f" + i + " c" + j + " negro\" " + 
						"style=\"background-color: #777777;\" " +
						"onmouseover=\"dime_pos("+ i +", "+ j +"), limpia_fondo()\" " +
						"onclick=\"pon_pos(), get_ficha(this)\" " +
						"ondblclick=\"quitar_ficha(this)\" " +
						"></div>";
		}
	}
}

function resetear () {

	// Resetea varias variables para que el jugador pueda empezar de nuevo
	for(var i = 0; i < DIM; i++) {
		for(var j = 0; j < DIM; j++) {
			var celda = document.getElementsByClassName(" f"+ i +" c"+ j )[0].innerHTML = "";
		}
	}
	limpia_fondo ();

	document.getElementsByClassName('posicion')[0].innerHTML = "";
	document.getElementsByClassName('posicion')[1].innerHTML = "";
	document.getElementsByClassName('pieza')[0].innerHTML = "";
	document.getElementsByClassName('color')[0].innerHTML = "";
	document.getElementById('posicion_cursor').innerHTML = "- Fila donde estas =<br>- Columna donde estas = ";

	document.getElementById('blancas').innerHTML = "- Blancas:<br>";
	document.getElementById('negras').innerHTML = "- Negras:<br>";

	pon_fichas_menu ()
}

function limpia_fondo () {

	// Limpia el fondo y lo vuelva a blanco y negro
	for(var i = 0; i < DIM; i++) {
		for(var j = 0; j < DIM; j++) {
			var celda = document.getElementsByClassName(" f"+ i +" c"+ j )[0];
			if(celda.className.indexOf('blanco') != -1)
				celda.style.backgroundColor = "";
			else
				celda.style.backgroundColor = "#777777";
		}
	}
}

function ayuda () {

	// Muestra una ayuda
	alert("Ajedrez:\n\n" +
		"Pon las piezas en el tablero escojiendo la ficha y la posicion clicando en el tablero.\n" +
		"Si quieres quitar la ficha haz doble click en ella.");
}

function main () {

	dibuja_tablero()
	pon_fichas_menu()
}