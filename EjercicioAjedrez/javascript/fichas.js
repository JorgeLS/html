function pon_fichas_menu () {

	// Pone las fichas blancas en el menú de fichas
	menu = document.getElementById('blancas');

	for (var i = 0; i < fichas.length; i++)
		menu.innerHTML += "<img src=\"imagenes/"+ fichas[i] +"-blanco.png\" " +
				"name=\""+ fichas[i] +" blanco\"" +
				"onclick=\"escoje_ficha(" + i + ", 'blanco')\" >";

	// Pone las fichas negras en el menú de fichas
	menu = document.getElementById('negras');

	for (var i = 0; i < fichas.length; i++)
		menu.innerHTML += "<img src=\"imagenes/"+ fichas[i] +"-negro.png\" " +
				"name=\""+ fichas[i] +" negro\" " +
				"onclick=\"escoje_ficha("+ i +", 'negro')\" >";
}

function escoje_ficha (x, col) {

	// Cuando apretas en alguna ficha se llama a esta funcion que guarda el nombre de la ficha y su color
	nom_ficha = fichas[x];
	color = col;

	// Muestra el nombre de la ficha y su color que se ha elegido
	document.getElementsByClassName('pieza')[0].innerHTML = nom_ficha.toUpperCase()
	document.getElementsByClassName('color')[0].innerHTML = color.toUpperCase()
}

function poner_ficha () {

	// Al apretar el boton "Poner Ficha" pone la ficha en el tablero
	// Pero primero hay que definir la ficha , la posicion y el color
	if ( fichas.indexOf(nom_ficha) != -1 && color != -1 && fila != -1 && columna != -1 ) {
		document.getElementsByClassName("f"+fila+" c"+columna)[0].innerHTML = "<img " +
				"src=\"imagenes/" + nom_ficha + "-" + color + ".png\"> ";
	}
}

function quitar_ficha (x) {

	// Al hacer doble click a una ficha en el tablero se llama a esta funcion la cual la quita
	// Para saber que fila y 
	var borra_fila =  x.className.slice(8, 10);
	var borra_columna = x.className.slice(11, 13);

	document.getElementsByClassName(borra_fila + " " + borra_columna)[0].innerHTML = "";
}

function get_ficha (x) {

	// Cuando una ficha esta en el tablero si se clica en ella
	// Se cojerá una su fila, su columna, su nombre y su color
	if(x.innerHTML != "") {

		// Para cojer la fila y la columna uso el slice en el class para cojer solo los numeros
		// Ej: Si class="columna f0 c5" solo cojerá el 0 y el 5
		fila = parseInt(x.className.slice(9, 10));
		columna = parseInt(x.className.slice(12, 13));

		// Se coje el nombre de una ficha mirando la etiqueta de la imagen y buscando si tiene el mismo nombre que en el array de fichas
		for (var i = 0; i < fichas.length; i++) {
			if ( x.innerHTML.indexOf(fichas[i]) != -1 ) {
				num_ficha = i;
				break;
			}
		}

		// Se busca como en la ficha en la imagen hay la palabra blanco o negro
		if ( x.innerHTML.indexOf("blanco") != -1 )
			color = "blanco";
		else
			color = "negro";

		// Limpia el fondo para no mostrar movimientos de otras fichas
		limpia_fondo ();
		// Muestra el movimiento de esta ficha
		movimientos (x);
	}
}