function dime_pos (f, c) {

	// Si pasa por encima de una celda te dice la celda en la que estas
	get_fila = f;
	get_columna = c;

	// Te muesta la celda en la que estas
	document.getElementById('posicion_cursor').innerHTML =
		"- Fila donde esta el cursor = " + get_fila +
		"<br>" +
		"- Columna donde esta el cursor = " + get_columna;
}

function pon_pos () {

	// Si clica la celda te pondrá la fila y la columna en la que esta la celda y la guarda
	fila = get_fila;
	columna = get_columna;

	// Te mostrará la posición en el menú
	document.getElementsByClassName('posicion')[0].innerHTML = fila;
	document.getElementsByClassName('posicion')[1].innerHTML = columna;
}